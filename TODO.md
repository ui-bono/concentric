- Make it possible to position the component statically, so that the origin respects the document flow.
    So far, only positioning using cx and cy props is implemented, which is absolute under the hood.s
- Provide a flag that disables drag cancellation upon dragging across the center.
- Decompose internal circle SVGs into a helper component.
- Add an option to provide custom children (not just circles).