# Concentric

A drag slider component in the form of concentric circles
appearing or disappearing as you drag away or to the center, respectively.

## Showcase

[...you can find here.](https://ui-bono.gitlab.io/concentric-showcase/)

## Installation

```
npm i @ui-bono/concentric
```

## Contributing

You are most welcome to contribute.  
Test cases would be particularly useful at the moment, to cover existing functionality.
Issues are being opened as well, and await our attention.

## License

MIT

## Disclaimer

This package is still in alpha. Here be dragons.
