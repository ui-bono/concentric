const path = require("path");
const glob = require("glob");
module.exports = {
  entry: glob.sync("./src/**/*jasmine.js?(x)"),
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      }
    ],
  },
  resolve: {
    extensions: ["*", ".js", ".jsx", ".ts", ".tsx"],
  },
  output: {
    path: path.resolve(__dirname, "./out_test"),
    filename: "test.js",
  },
  devServer: {
    static: path.resolve(__dirname, "./out_test"),
  },
};