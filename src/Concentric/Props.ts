export interface Props {
    cx: number,
    cy: number,
    scale: number,
    minSize: number,
    maxSize: number,
    maxCircles: number,
    padding: number,
    minValue: number,
    maxValue: number,
    value: number,
    onValueChange: (value: number) => void,
    stroke: string,
    strokeWidth: number
};