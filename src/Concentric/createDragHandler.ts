import { DragState } from "@use-gesture/react";
const createDragHandler = (
    cx: number,
    cy: number,
    maxDragSurfaceSize: number,
    callback: (param: number) => any
) => {
    const handler = ({
        xy: [x, y],
        first,
        cancel,
        canceled,
        event,
        memo
    }: DragState) => {
        event.stopPropagation();
        if (canceled) {
            return;
        } else if (first) {
            const slopeNumerator = Math.round(y - cy);
            const slopeDenominator = Math.round(x - cx);
            // const radialRaySlope = slopeNumerator / slopeDenominator;
            const radialHorizonSlope = - (slopeDenominator / slopeNumerator);
            const radialHorizonYIntercept = cy - radialHorizonSlope * cx;
            const radialHorizon = (px: number): number => 
                radialHorizonSlope * px + radialHorizonYIntercept;
            memo = {
                radialHorizon,
                radialOrientation: Math.sign(radialHorizon(x) - y)
            };
        } else if (memo.radialOrientation * (memo.radialHorizon(x) - y) <= 0) {
            cancel();
        };
        const distance = Math.sqrt((x - cx) ** 2 + (y - cy) ** 2);
        const direction = Math.sign(memo?.distance ? distance - memo.distance : 0);
        //The commented ratio also works, but needs bigger divisor (maxDragSurfaceSize * 4)
        //const ratio = Math.sqrt(dx ** 2 + dy ** 2) / (maxDragSurfaceSize  * 4);
        const ratio = memo?.distance ? Math.abs(memo.distance - distance) / maxDragSurfaceSize : 0;
        ratio && callback(direction * ratio);
        memo = ({ ...memo, distance, x, y });
        return memo;
    };
    return handler;
};
export default createDragHandler;