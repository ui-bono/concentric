import React, { useRef, LegacyRef } from "react";
import { Target, useGesture } from "@use-gesture/react";
import throttle from "lodash-es/throttle";
import createDragHandler from "./createDragHandler";
import { Props } from "./Props";
const Concentric = ({
    cx,
    cy,
    scale,
    minSize,
    maxSize,
    maxCircles,
    padding,
    minValue,
    maxValue,
    value,
    onValueChange,
    stroke,
    strokeWidth
}: Props) => {
    const valueRange = maxValue - minValue;
    const normalizeValue = (value: number) => value - minValue;
    const sizeIncrement = scale * (maxSize - minSize - padding) / (maxCircles - 1) / 2;
    const getNumberOfCircles = (value: number) => {
        const circles = Math.round(normalizeValue(value) / valueRange * maxCircles);
        return (circles <= 0 ? 1
            : circles > maxCircles ? maxCircles
            : circles);
    };
    const treeNodeCircleControlRef = useRef() as LegacyRef<SVGSVGElement> | undefined;
    const throttledPropagateValue = throttle((relativeIncrement: number) => {onValueChange(value + relativeIncrement * valueRange)});
    useGesture({
        onDrag: createDragHandler(cx, cy, maxSize, num => throttledPropagateValue(num) )
    }, { target: treeNodeCircleControlRef as Target });
    return <svg 
        xmlns="http://www.w3.org/svg/2000"
        ref={treeNodeCircleControlRef}
        width={scale * maxSize}
        height={scale * maxSize}
        style={{
            touchAction: "none",
            position: "absolute",
            left: scale * cx - maxSize / 2,
            top: scale * cy - maxSize / 2
        }}>
            {
                new Array(getNumberOfCircles(value)).fill(1)
                .map((_, i) => <circle
                    key={`concentric-circle-${i}`}
                    cx={scale * maxSize / 2}
                    cy={scale * maxSize / 2}
                    r={scale * minSize / 2 + i * sizeIncrement}
                    fill={"transparent"}
                    stroke={stroke}
                    strokeWidth={scale * strokeWidth} />)
            }
        </svg>
};
export default Concentric;