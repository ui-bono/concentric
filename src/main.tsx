import React, {useState} from "react";
import { createRoot } from "react-dom/client";
import { useWindowSize }  from "react-use";
import { Concentric } from "./Concentric";
const root = createRoot(document.getElementById("root")!);
const Demo = () =>  {
    const [value, setValue] = useState(0);
    const { width, height } = useWindowSize();
    return <>
        <h1 style={{ marginTop: height / 10 }}>@ui-bono/concentric</h1>
        <Concentric
            cx={width / 2}
            cy={height/ 1.8}
            scale={1} 
            minSize={50}
            maxSize={200}
            maxCircles={20}
            padding={2}
            minValue={-20}
            maxValue={20}
            value={value}
            onValueChange={value => { setValue(value); }}
            stroke={"burlywood"}
            strokeWidth={2} />
    </>
}
root.render(
    <React.StrictMode>
        <Demo />
    </React.StrictMode>
);